//*****************************//
//Class pca: finds the principal components (or the fisrst n principal components) of a training set.
//Method:
//- find the direction that minimizes the distance of the points, this is the principal component of the set;
//- project the data in the hyperplaneplane orthogonal to the direction just found, and repeat the minimization to find the next PC;
//- iterate until all PCs are found or until the first n PCs are found;
//Inheritance: none;
//Author: Derin Lucio
//*****************************//

//*******CONSTRUCTORS:********//
//@n: number of dimension of the set;
//pca(int n)

//@n: number of dimension of the set;
//@verbose: enable or disable verbose printing (enabled if verbose==true);
//pca(int n, bool verbose)

//**********SETTERS:**********//
//void setDim(int dim) -> Set the dimension of the set;
//void enablePrinting(bool enable) -> Enable or disable verbose printing (enablet if verbose == true);

//**********GETTERS:**********//
//double getN() -> Get the dimension of the set;
//bool getOutputMode() -> Get the output mode;

//**********METHODS:**********//
//Get all the PCs (the number of PCs corresponds to the dimension of the set);
//@set: set of n-dimensional points;
//std::vector<std::vector<double>> getPC(const std::vector<std::vector<double>> &set);

//Get the first nPC PCs;
//@set: set of n-dimensional points;
//@nPC: number of first desired PC;
//std::vector<std::vector<double>> getNPC(const std::vector<std::vector<double>> &set, int nPC);
//****************************//

//Notation:
//dimension of the set: number of coordinates to define a point (not the number of points in the set);
//PC: principal component;
//PCs: principal components;
//PCA: principal component analysis;

#ifndef _PCA
#define _PCA

#include <vector>

class pca {
  private:
    //dimension of the set
    int m_n;
    //output mode
    bool m_verbose;

  public:
    //Constructors:
    pca(int n) : m_n(n) {}
    pca(int n, bool verbose) : m_n(n), m_verbose(verbose) {}

    //Methods:
    std::vector<std::vector<double>> getPC(const std::vector<std::vector<double>> &set);
    std::vector<std::vector<double>> getNPC(const std::vector<std::vector<double>> &set, int nPC);

    //Setters:
    void setDim(int dim) { m_n = dim; }
    void enablePrinting(bool enable) { m_verbose = enable; }

    //Getters:
    double getN() { return m_n; }
    bool getOutputMode() { return m_verbose; }
};

#endif //_PCA