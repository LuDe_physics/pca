//Header:
#include "pca.h"
//c++
#include <cmath>
#include <iostream>
//ROOT
#include <Math/Factory.h>
#include <Math/Functor.h>
#include <Math/Minimizer.h>
#include <TMath.h>
#include <TMinuit.h>

//enable/disable debug mode: enabled if true;
#define debug true

//Namespace with functions and objects needed for PCA
namespace pcaFun {
//dimension of the set
int setDimension;
//set on which minimize the distance (it will be projected on the new hyperplane after each iteration)
std::vector<std::vector<double>> set;

//dot product between two std::vector;
//@x: first vector;
//@y: second vector;
double dot(const std::vector<double> &x, const std::vector<double> &y) {
//if in debug mode, check for corret dimensions
#if debug
    if (x.size() != y.size()) {
        std::cerr << "Error in dot: can't evaluate dot product between vectors of different sizes.\n";
        return 0.;
    }
#endif
    //evaluating dot product
    int n = x.size();
    double dot = 0.;
    for (int i = 0; i < n; i++)
        dot += x[i] * y[i];
    return dot;
}

//normalize the vector x;
//@x: vector to be normalized (it will be modified);
void normalize(std::vector<double> &x) {
    double norm = std::sqrt(dot(x, x));
    for (auto &p : x)
        p = p / norm;
    return;
}

//distance of a point from a given direction;
//@ur: unit vector of the direction;
//@P: coordinates of the point
double dist(const std::vector<double> &ur, const std::vector<double> &P) {
//if in debug mode, check for correct dimensions and norm
#if debug
    if (ur.size() != P.size()) {
        std::cerr << "Error in dist: can't calculate distances of point from a versor with different dimensions;\n";
        return 0.;
    }
    if (std::round(dot(ur, ur)) != 1) {
        std::cerr << "Error in dist: ur is not a unit vector.\n";
        return 0.;
    }
#endif

    //evaluating distance between P and its projection P' on the direction ur
    double dist = 0;
    int n = ur.size();
    double Pproj = dot(ur, P);
    for (int i = 0; i < n; i++)
        dist += std::pow(P[i] - Pproj * ur[i], 2);

    return std::sqrt(dist);
}

//error function to be minimized (sum of the distances of the points from the direction);
//@par: parameters for the minimizer, i.e. the coefficient of the linear combination of the original coordinates of the set;
double error(const double *par) {
    double e = 0.;
    //arbitrary direction modified by the minimizer in order to minimize the error
    std::vector<double> ur(pcaFun::setDimension);
    for (int i = 0; i < pcaFun::setDimension; i++)
        ur[i] = par[i];
    //normalizing ur to unit vector
    pcaFun::normalize(ur);
    //evaluating the sum of the distances
    for (const auto &p : pcaFun::set) {
        e += pcaFun::dist(ur, p);
    }
    return e;
}

//project the set on the hyperplane orthogonal to the direction u;
//@set: the set to be projected;
//@u: unit vector of the direction;
std::vector<std::vector<double>> proj(const std::vector<std::vector<double>> &set, const std::vector<double> &u) {
//if in debug mode, check for correct norm
#if debug
    if (std::round(dot(u, u)) != 1) {
        std::cerr << "Error in proj: u is not a unit vector.\n";
        return {{0.}};
    }
#endif

    //projected set
    std::vector<std::vector<double>> projSet;
    projSet.reserve(set.size());

    //for every point, finding the intersection between the hyperplane orthogonal to u and the
    //straight line orthogonal to u and containing the point (i.e. the orthogonal projection of the point on the hyperplane)
    //t is the parameter of the straight line in parametric notation
    for (const auto &p : set) {
        double t = -dot(u, p);
        std::vector<double> temp(p.size());
        for (int i = 0; i < p.size(); i++)
            temp[i] = p[i] + u[i] * t;
        projSet.emplace_back(temp);
    }
    //moving the set using the move semantic of std::vector
    return projSet;
}

}; // namespace pcaFun

//get the principal components of the set;
//@set: coordinates of the points (std::vector of std::vectors of m_n components each);
//returns: the PC orthogonal basis unit vectors (a std::vector of std::vectors of n components each);
std::vector<std::vector<double>> pca::getPC(const std::vector<std::vector<double>> &set) {
    //final versors
    std::vector<std::vector<double>> versors(m_n);
    //Using ROOT's TMinuit minimizer
    ROOT::Math::Minimizer *min = ROOT::Math::Factory::CreateMinimizer("Minuit", "");
    //selecting the output mode
    if (m_verbose)
        min->SetPrintLevel(1);
    else
        min->SetPrintLevel(-1);

    //copying the set
    pcaFun::set = set;
    //setting the dimension of the set
    pcaFun::setDimension = m_n;

    //iteration to find the n PCs
    for (int i = 0; i < m_n; i++) {
        //search of the i-th PC
        //versor of the i-th PC
        std::vector<double> iPC(m_n);
        //Setting up the minimizer function
        ROOT::Math::Functor f(&pcaFun::error, m_n);
        min->SetFunction(f);
        //Setting up the minimizer variables
        //i.e. the coefficient of the linear combination of the original coordinates of the set
        double step = 0.1;
        for (int var = 0; var < m_n; var++)
            min->SetVariable(var, "", 1, step);
        //minimizing
        min->Minimize();
        //Getting the coefficient of the i-th PC vector
        auto result = min->X();
        for (int j = 0; j < m_n; j++)
            iPC[j] = result[j];
        //creating the i-th PC unit vector and saving it
        pcaFun::normalize(iPC);
        versors[i] = iPC;

        //projecting the data in the hyperplaneplane orthogonal to iPC
        pcaFun::set = pcaFun::proj(pcaFun::set, iPC);
    }
    return versors;
}

//get the first nPC principal components of the set;
//@set: coordinates of the points (std::vector of std::vectors of m_n components each);
//@nPC: number of desired first principal components;
//returns: the first nPC orthogonal unit vectors of the PC basis (a std::vector of std::vectors of n components each);
std::vector<std::vector<double>> pca::getNPC(const std::vector<std::vector<double>> &set, int nPC) {
    //final versors
    std::vector<std::vector<double>> versors(m_n);
    //Using ROOT's TMinuit minimizer
    ROOT::Math::Minimizer *min = ROOT::Math::Factory::CreateMinimizer("Minuit", "");
    //selecting the output mode
    if (m_verbose)
        min->SetPrintLevel(1);
    else
        min->SetPrintLevel(-1);

    //copying the set
    pcaFun::set = set;
    //setting the dimension of the set
    pcaFun::setDimension = m_n;

    //iteration to find the n PCs
    for (int i = 0; i < nPC; i++) {
        //search of the i-th PC
        //versor of the i-th PC
        std::vector<double> iPC(m_n);
        //Setting up the minimizer function
        ROOT::Math::Functor f(&pcaFun::error, m_n);
        min->SetFunction(f);
        //Setting up the minimizer variables
        //i.e. the coefficient of the linear combination of the original coordinates of the set
        double step = 0.1;
        for (int var = 0; var < m_n; var++)
            min->SetVariable(var, "", 1, step);
        //minimizing
        min->Minimize();
        //Getting the coefficient of the i-th PC vector
        auto result = min->X();
        for (int j = 0; j < m_n; j++)
            iPC[j] = result[j];
        //creating the i-th PC unit vector and saving it
        pcaFun::normalize(iPC);
        versors[i] = iPC;

        //projecting the data in the hyperplaneplane orthogonal to iPC
        pcaFun::set = pcaFun::proj(pcaFun::set, iPC);
    }
    return versors;
}
