## C++ class to evaluate the principal components of a set
### This class makes use of CERN's ROOT classes

### Algorithm:
 - Evaluate the direction that minimizes the sum of the distances of the points, i.e. the principal component;
 - Project all the points in the hyperplane orthogonal to the direction found;
 - Iterate the process to find all the principal components.

### Minimizer: TMinuit

### Methods:
 - getPC: returns a ```std::vector<std::vector<double>>``` containing the unit vectors of the principal components of the set;
 - getNPC: returns a ```std::vector<std::vector<double>>``` containing the unit vectors of the first n principal components of the set;