//class pca
#include "../pca.h"
//c++
#include <iostream>
#include <vector>
//ROOT
#include <TGraph2D.h>
#include <TRandom3.h>
#include <TF2.h>
#include <TCanvas.h>
#include <TApplication.h>

int main()
{
    TApplication app("app",NULL,NULL);

    //Generating a set of gaussian data with:
    //sigma = 1 on x direction
    //sigma = 0.5 on y direction
    //sigma = 0.1 on z direction
    //These are the three PCs
    TRandom3 r;
    r.SetSeed(12345);
    int nPoints = 100000;
    std::vector<std::vector<double>> set(nPoints);
    for(int i=0;i<nPoints;i++)
        set[i] = {r.Gaus(0,1),r.Gaus(0,0.5),r.Gaus(0,0.1)};
    
    //evaluating PCs
    pca pcaObj(3);
    pcaObj.enablePrinting(false);
    auto versors = pcaObj.getPC(set);
    std::cout << "Versors obtained by PCA:\n";
    for(int i=0;i<3;i++)
        std::cout << "u"<< i << " = (" << versors[i][0] << "," << versors[i][1] << "," << versors[i][2] << ");\n";
    std::cout << "Rounded Versors obtained by PCA:\n";
    for(int i=0;i<3;i++)
        std::cout << "u"<< i << " = (" << std::round(versors[i][0]) << "," << std::round(versors[i][1]) << "," << std::round(versors[i][2]) << ");\n";
    std::cout << "Real PC:\n";
    std::cout << "u"<< 0 << " = (" << 1 << "," << 0 << "," << 0 << ");\n";
    std::cout << "u"<< 1 << " = (" << 0 << "," << 1 << "," << 0 << ");\n";
    std::cout << "u"<< 2 << " = (" << 0 << "," << 0 << "," << 1 << ");\n";
    std::cout << "The discrepancy between real and obtained value is due to the statistical fluctuations.\n";
    
    //evaluating only the first PC
    auto firstPC = pcaObj.getNPC(set,1);
    std::cout << "First principal component of the set:\n";
    std::cout << "u"<< 0 << " = (" << firstPC[0][0] << "," << firstPC[0][1] << "," << firstPC[0][2] << ");\n";

    //Graphical output
    TGraph2D *setGraph = new TGraph2D();
    for(int i=0;i<nPoints;i++)
        setGraph->SetPoint(i,set[i][0],set[i][1],set[i][2]);
    TGraph2D *firstPCpoints = new TGraph2D();
    double step = 1e-3;
    for(int i=-1000;i<1000;i++)
        firstPCpoints->SetPoint(i+1000,firstPC[0][0]*i*step,firstPC[0][1]*i*step,firstPC[0][2]*i*step);
    for(int i=-1000;i<1000;i++)
        firstPCpoints->SetPoint(i+3000,versors[1][0]*i*step,versors[1][1]*i*step,versors[1][2]*i*step);
    for(int i=-100;i<100;i++)
        firstPCpoints->SetPoint(i+5000,versors[2][0]*i*step,versors[2][1]*i*step,versors[2][2]*i*step);
    
    setGraph->SetTitle("Set and it's first components");
    firstPCpoints->SetMarkerStyle(20);
    firstPCpoints->SetMarkerSize(0.1);
    firstPCpoints->SetMarkerColor(kRed);

    setGraph->Draw("AP");
    firstPCpoints->Draw("SAME P");

    app.Run(true);
    return 0;
}
